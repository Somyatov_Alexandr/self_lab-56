import React, {Component} from 'react';
import Burger from "../../components/Burger/Burger";
import Info from "../../components/Info/Info";

class BurgerBuilder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ingredients: {
        Salad: 0,
        Bacon: 0,
        Cheese: 0,
        Meat: 0,
      }
    };

    this.price = {
      Salad: 5,
      Bacon: 30,
      Cheese: 20,
      Meat: 50,
      _defaultCost: 20
    }
  }

  moreIngredient = key => {
    let ingredient = this.state.ingredients[key];
    ingredient++;

    const ingredients = {...this.state.ingredients};
    ingredients[key] = ingredient;

    this.setState({ingredients});
  };

  lessIngredient = key => {
    let ingredient = this.state.ingredients[key];
    if (ingredient > 0) {
      ingredient--;
    }

    const  ingredients = {...this.state.ingredients};
    ingredients[key] = ingredient;

    this.setState({ingredients});
  };

  render() {
    return (
      <div>
        <Burger ingredients={this.state.ingredients}/>
        <Info defaultCost={this.price._defaultCost} ingredients={this.state.ingredients} price={this.price} more={this.moreIngredient.bind(this)} less={this.lessIngredient.bind(this)}/>
      </div>
    );
  }
}

export default BurgerBuilder;