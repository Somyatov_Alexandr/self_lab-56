import React from 'react';
import IngredControls from "./IngredControls/IngredControls";
import Price from "./Price/Price";

const Info = props => {
  const ingredControl = [];
  let price = props.defaultCost;

  for (let key in props.ingredients){
      ingredControl.push(<IngredControls less={() => props.less(key)} more={() => props.more(key)} key={key} type={key}/>)
      price += props.ingredients[key] * props.price[key];
  }


  return (
      <div className="burger__info">
        <Price price={price}/>
        {ingredControl}
      </div>
  );
};

export default Info