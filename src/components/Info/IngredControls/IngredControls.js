import React from 'react';
import './IngredControls.css';

const IngredControls = props => {
  return (
      <div className="ingred__item">
        <div className="ingred__name">{props.type}</div>
        <button onClick={props.less} className="ingred__less btn">Less -</button>
        <button onClick={props.more} className="ingred__more btn">More +</button>
      </div>
  );
};

export default IngredControls;